# nginx specific configuration

Specific congfiguration of nginx for two servers with static page and pass proxy.

Adopted for Openshift (non-root)

No persistent volume required

Define new build configuration
```
oc new-build https://slookin@bitbucket.org/slookin/openshift.git --name=ngnixproxy --context-dir=nginx_proxy --strategy=docker
```

Start app:

```
/oc new-app ngnixproxy
```

Rebuild:
```
/oc start-build ngnixproxy
```

Please setup two routes manually. (8080 / 8081)
