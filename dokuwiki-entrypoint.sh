#!/bin/bash
set -e

if [ ! -f /dokuwiki/data/_dummy ]; then
    echo "init volumes"
    cp -rp  /dokuwiki/data_initial/* /dokuwiki/data
    cp -rp /dokuwiki/lib/plugins_initial/* /dokuwiki/lib/plugins
    cp -rp /dokuwiki/lib/tpl_initial/* /dokuwiki/lib/tpl
    cp -rp /dokuwiki/conf_initial/* /dokuwiki/conf
fi


run_dokuwiki() {
#    chown -R apache:apache /var/www /var/lib/nginx

    exec /usr/bin/supervisord -c /etc/supervisord.conf
}

case "$1" in
    dokuwiki)
        shift 1
        run_dokuwiki "$@"
        ;;
    *)
        exec "$@"
esac
