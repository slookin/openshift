FROM centos:centos7

#ENV http_proxy=http://10.233.104.142:3128
#ENV https_proxy=https://10.233.104.142:3128
#ENV no_proxy='127.0.0.1'

ARG DOKUWIKI_VERSION=2017-02-19e
ARG DOKUWIKI_CSUM=09bf175f28d6e7ff2c2e3be60be8c65f

RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install epel-release && \
    yum -y --setopt=tsflags=nodocs install nginx && \
    yum -y --setopt=tsflags=nodocs install php-gd php-fpm wget supervisor && \
    yum clean all

RUN wget -q -O /dokuwiki.tgz "http://download.dokuwiki.org/src/dokuwiki/dokuwiki-$DOKUWIKI_VERSION.tgz" && \
    mkdir /dokuwiki && \
    tar -zxf dokuwiki.tgz -C /dokuwiki --strip-components 1 && \
    rm dokuwiki.tgz

COPY etc/ /etc/
COPY dokuwiki-entrypoint.sh /

#  for some reason nginx is still trying to create /var/log/nginx/error.log
#  even if error_log /dev/stderr is set
RUN cp -r /dokuwiki/data /dokuwiki/data_initial && cp -r /dokuwiki/lib/plugins /dokuwiki/lib/plugins_initial && cp -r /dokuwiki/lib/tpl /dokuwiki/lib/tpl_initial && cp -r /dokuwiki/conf /dokuwiki/conf_initial 

RUN chown -R apache:apache /dokuwiki && chmod -R go+rwx /var/log && chmod -R go+rwx /var/lib/nginx && chmod -R go+rwx /dokuwiki && chmod +x /dokuwiki-entrypoint.sh

USER 997


VOLUME ["/dokuwiki/data/","/dokuwiki/lib/plugins/","/dokuwiki/conf/","/dokuwiki/lib/tpl/"]
#"/var/log/"
EXPOSE 8080
ENTRYPOINT ["/dokuwiki-entrypoint.sh"]
CMD ["dokuwiki"]