## Nosqlclient (Formerly Mongoclient), MongoDB Management Tool

Cross-platform and self hosted, easy to use, MongoDB 3.4+ support and more features! 

Based on mongoclient/mongoclient:2.2.0 and modified for openshift platform

## Openshift

                         
Deployment from Dockerfile

Storage should be manually created: mongoclient-db

Create new build configuration
```
./oc new-build https://slookin@bitbucket.org/slookin/openshift.git --name=nosqlclient --context-dir=nosqlclient/ --strategy=docker
```

Start app:
```
/oc new-app nosqlclient
```

Rebuild:
```
/oc start-build nosqlclient
```

Setup persistent storages:
```
./oc volume dc/nosqlclient --add --overwrite -t pvc --name=nosqlclient-volume-1 --claim-name=mongoclient-db
```

Please setup route manually.
