# Dokuwiki for Openshift
Dokuwiki is simple Wiki engine. Learn more about it at http://dokuwiki.org.

Running containers on OpenShift comes with certain security and other
requirements. This repository contains:

* A Dockerfile for building an OpenShift-compatible Dokuwiki image
* Various scripts used in the Docker image
* Usage instructions

## Deployment from Dockerfile
Several storages should be manually created:
* dokuwiki-data
* dokuwiki-plugins
* dokuwiki-conf
* dokuwiki-tpl

Define new build configuration
```
oc new-build https://slookin@bitbucket.org/slookin/openshift.git --name=dokuwiki
```

Rebuild:
```
/oc start-build dokuwiki
```

Setup persistent storages:
```
/oc volume dc/dokuwiki --add --overwrite -t pvc --name=dokuwiki-volume-1 --claim-name=dokuwiki-data
/oc volume dc/dokuwiki --add --overwrite -t pvc --name=dokuwiki-volume-2 --claim-name=dokuwiki-plugins
/oc volume dc/dokuwiki --add --overwrite -t pvc --name=dokuwiki-volume-3 --claim-name=dokuwiki-conf
/oc volume dc/dokuwiki --add --overwrite -t pvc --name=dokuwiki-volume-4 --claim-name=dokuwiki-tpl
```

Please setup route manually.
